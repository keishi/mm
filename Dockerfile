FROM ubuntu
MAINTAINER Keishi Hattori <keishi.hattori@gmail.com>

RUN echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y python-dev python-pip
RUN apt-get install -y libjpeg-dev zlib1g-dev libpng12-dev libwebp-dev
RUN apt-get install -y libapache2-mod-wsgi

ADD . /mm

ENV MM_SETTINGS /mm/production_settings.py

RUN pip install -r /mm/requirements.txt

RUN mkdir /mm-data
RUN chown www-data /mm-data
RUN su www-data -c "python /mm/manager.py setup"

VOLUME ["/mm-data"]

RUN rm /etc/apache2/sites-enabled/000-default
RUN ln -s /mm/mm-site /etc/apache2/sites-enabled

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2

EXPOSE 80

ENTRYPOINT ["/usr/sbin/apache2"]
CMD ["-D", "FOREGROUND"]
