import os
import sys

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))

sys.path.append(PROJECT_DIR)

from mm import app as application
