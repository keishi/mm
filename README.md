mm
==

Personal media manager.

## Getting Started

    sudo docker build -t mm/server .
    sudo docker run -d -p 80:80 mm/server


    mkdir /tmp/mm-data; python manager.py setup;
    python manager.py add <DIR_CONTAINING_IMAGES>
    sudo docker build -t mm/server .
    sudo docker run -d -p 80:80 -v /tmp/mm-data:/mm-data mm/server
