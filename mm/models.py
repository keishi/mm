from mm import app, db
from flask import Flask
from contextlib import closing
import magic
import hashlib
import shutil
import os
from PIL import Image
import unicodedata
import re
import sqlite3

class Tag(object):
    def __init__(self):
        self.id = None
        self.name = None
    
    @classmethod
    def from_id(cls, id):
        row = db.query('SELECT * FROM tag WHERE id = ? LIMIT 1', (str(id),), True)
        if row is None:
            return None
        return Tag.from_row(row)

    @classmethod
    def from_row(cls, row):
        tag = Tag()
        tag.id = row['id']
        tag.name = row['name']
        return tag
     
    @classmethod
    def from_name(cls, name):
        name = Tag.normalize_name(name)
        row = db.query('SELECT * FROM tag WHERE name = ? LIMIT 1', (name,), True)
        if row:
            return Tag.from_row(row)
        tag = Tag()
        tag.name = name
        tag.insert()
        return tag
    
    @classmethod
    def lookup(cls, name):
        name = Tag.normalize_name(label)
        row = db.query('SELECT * FROM tag WHERE name = ? LIMIT 1', (name,), True)
        if row:
            return Tag.from_row(row)
        return None

    def tagged(self):
        rows = db.query('SELECT * FROM tagmap, media WHERE tagmap.tag_id = ? AND tagmap.media_id = media.id ORDER BY media.id', (str(self.id),))
        return [Media.from_row(row) for row in rows]

    def insert(self):
        cur = db.get().execute('INSERT INTO tag (name) VALUES (?)', (self.name,))
        self.id = cur.lastrowid

    def map(self, media):
        db.get().execute('INSERT INTO tagmap (tag_id, media_id) VALUES (?, ?)', (self.id, media.id))

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'tagged': [media.to_dict() for media in self.tagged()]
        }

    @staticmethod
    def normalize_name(name):
        name = unicodedata.normalize('NFKC', name).lower()
        return re.sub('\s', '_', name)


class Media(object):
    def __init__(self):
        self.id = None
        self.hash = None
        self.title = None
        self.extension = None
    
    @staticmethod
    def mime_type_to_extension(mime_type):
        #if mime_type == 'image/gif':
        #    return 'gif'
        if mime_type == 'image/jpeg':
            return 'jpg'
        if mime_type == 'image/png':
            return 'png'
        if mime_type == 'image/webp':
            return 'webp'
        return None
    
    @staticmethod
    def create_thumbnail(infile, outfile, quality=85):
        im = Image.open(infile)
        im = im.convert('RGB')
        im.thumbnail((256, 256), Image.ANTIALIAS)
        im.save(outfile, 'WEBP', quality=quality)
    
    @staticmethod
    def get_file_extension(filename):
        mime_type = magic.from_file(filename, mime=True)
        return Media.mime_type_to_extension(mime_type)
    
    @staticmethod
    def get_file_hash(filename):
        f = open(filename, 'rb')
        sha1 = hashlib.sha1()
        while True:
            data = f.read(255)
            if not data:
                break
            sha1.update(data)
        f.close()
        return sha1.hexdigest()
    
    def relative_thumbnail_dir(self):
        id_str = '%09d' % self.id
        return os.path.join('256', id_str[0:3], id_str[3:6])
    
    def thumbnail_dir(self):
        return os.path.join(app.config['THUMBNAIL_DIR'], self.relative_thumbnail_dir())
    
    def relative_thumbnail_path(self):
        return os.path.join(self.relative_thumbnail_dir(), '%09d.webp' % self.id)
    
    def thumbnail_path(self):    
        return os.path.join(app.config['THUMBNAIL_DIR'], self.relative_thumbnail_path())
    
    def relative_master_dir(self):
        id_str = '%09d' % self.id
        return os.path.join(id_str[0:3], id_str[3:6])
    
    def master_dir(self):
        return os.path.join(app.config['MASTER_DIR'], self.relative_master_dir())
    
    def relative_master_path(self):
        return os.path.join(self.relative_master_dir(), '%09d.%s' % (self.id, self.extension))
    
    def master_path(self):
        return os.path.join(app.config['MASTER_DIR'], self.relative_master_path())
    
    @classmethod
    def from_id(cls, id):
        row = db.query('SELECT * FROM media WHERE id = ? LIMIT 1', (str(id),), True)
        if row is None:
            return None
        return Media.from_row(row)
    
    @classmethod
    def from_row(cls, row):
        media = Media()
        media.id = row['id']
        media.hash = row['hash']
        media.extension = row['extension']
        media.title = row['title']
        return media

    def insert(self):
        try:
            cur = db.get().execute('INSERT INTO media (hash, extension, title) VALUES (?, ?, ?)', (self.hash, self.extension, self.title))
        except sqlite3.IntegrityError:
            return
        self.id = cur.lastrowid
    
    def prepare_thumbnail(self, filename):
        if self.id is None:
            raise Exception('Media must have an id. Call insert() first.')
        try:
            os.makedirs(self.thumbnail_dir())
        except OSError, e:
            if e.errno != 17:
                raise
        try:
            Media.create_thumbnail(filename, self.thumbnail_path())
        except IOError, e:
            print "THUMBNAIL FAILED: %s <%s>" % (filename, str(e))
    
    def prepare_master(self, filename):
        if self.id is None:
            raise Exception('Media must have an id. Call insert() first.')
        try:
            os.makedirs(self.master_dir())
        except OSError, e:
            if e.errno != 17:
                raise
        shutil.copyfile(filename, self.master_path())
    
    def to_dict(self):
        return {
            'id': self.id,
            'hash': self.hash,
            'extension': self.extension,
            'title': self.title,
            'thumbnail': '/thumbnail/' + self.relative_thumbnail_path(),
            'master': '/master/' + self.relative_master_path(),
        }
