from mm import app, db
from mm.models import Media
from flask.ext import restful
from flask import send_from_directory

api = restful.Api(app)

class MediaResource(restful.Resource):
    def get(self, media_id):
        media = Media.from_id(media_id)
        if media is None:
            restful.abort(404, message="Media {} doesn't exist".format(media_id))
        return media.to_dict()

class MediaListResource(restful.Resource):
    def get(self):
        rows = db.query('SELECT * FROM media ORDER BY id')
        return [Media.from_row(row).to_dict() for row in rows]

class TagResource(restful.Resource):
    def get(self, tag_id):
        tag = Tag.from_id(tag_id)
        if tag is None:
            restful.abort(404, message="Tag {} doesn't exist".format(tag_id))
        return tag.to_dict()

class TagListResource(restful.Resource):
    def get(self):
        rows = db.query('SELECT * FROM tag ORDER BY id')
        return [Tag.from_row(row).to_dict() for row in rows]

api.add_resource(MediaResource, '/media/<int:media_id>')
api.add_resource(MediaListResource, '/media')
api.add_resource(TagResource, '/tag/<int:tag_id>')
api.add_resource(TagListResource, '/tag')

if app.debug:
    @app.route('/thumbnail/<path:filename>')
    def send_thumbnail(filename):
        return send_from_directory(app.config['THUMBNAIL_DIR'], filename)

    @app.route('/master/<path:filename>')
    def send_master(filename):
        return send_from_directory(app.config['MASTER_DIR'], filename)
