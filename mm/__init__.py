from flask import Flask

app = Flask(__name__)
app.config.from_object('mm.default_settings')
app.config.from_envvar('MM_SETTINGS', silent=True)

import mm.models
import mm.api
import mm.db
