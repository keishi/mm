var MM = {};

MM.enclosingNodeOrSelfWithClass = function(selfNode, className) {
    for (var node = selfNode; node && node !== selfNode.ownerDocument; node = node.parentNode) {
        if (node.nodeType === Node.ELEMENT_NODE && node.classList.contains(className))
            return node;
    }
    return null;
};

MM.cumulativeOffset = function(element) {
  var valueT = 0;
  var valueL = 0;
  if (element.parentNode) {
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
  }
  return [valueL, valueT];
};

MM.BrowserCell = function() {
  if (!MM.BrowserCell.template) {
    var element = document.createElement("div");
    element.classList.add("mm-image-browser-cell");
    var thumbnailContainerElement = document.createElement("div");
    thumbnailContainerElement.classList.add("mm-image-browser-cell-thumbnail-container");
    element.appendChild(thumbnailContainerElement);
    var labelElement = document.createElement("div");
    labelElement.classList.add("mm-image-browser-cell-label");
    element.appendChild(labelElement);
    MM.BrowserCell.template = element;
  }
  this.element = MM.BrowserCell.template.cloneNode(true);
  this.thumbnailContainerElement = this.element.children[0];
  this.labelElement = this.element.children[1];
  this.thumbnailSrc = null;
  this.thumbnailElement = null;
  this.thumbnailDownloader = null;
  this.priority = MM.BrowserCell.Priority.LOW;
  this.zoomLevel = 1.0;
};

MM.BrowserCell.prototype.setPosition = function(x, y) {
  var style = this.element.style;
  if (x !== null) {
    style.left = x + "px";
  }
  if (y !== null) {
    style.top = y + "px";
  }
};

MM.BrowserCell.prototype.setZoomLevel = function(zoomLevel) {
  this.zoomLevel = zoomLevel;
  var thumbnailSize = MM.BrowserCell.thumbnailSizeAtZoomLevel(zoomLevel);
  this.thumbnailContainerElement.style.width = thumbnailSize + "px";
  this.thumbnailContainerElement.style.height = thumbnailSize + "px";
  this.labelElement.style.width = thumbnailSize + "px";
};

MM.BrowserCell.prototype.loadThumbnail = function() {
  this.thumbnailElement = new Image();
  this.thumbnailElement.onload = this.onThumbnailLoad.bind(this);
  this.thumbnailElement.onerror = this.onThumbnailError.bind(this);
  this.thumbnailElement.src = this.thumbnailSrc;
};

MM.BrowserCell.prototype.onThumbnailLoad = function() {
  delete this.thumbnailElement.onload;
  delete this.thumbnailElement.onerror;
  this.thumbnailContainerElement.appendChild(this.thumbnailElement);
  if (this.thumbnailDownloader) {
    this.thumbnailDownloader.onThumbnailLoadDone(this);
  }
};

MM.BrowserCell.prototype.onThumbnailError = function() {
  delete this.thumbnailElement.onload;
  delete this.thumbnailElement.onerror;
  this.element.classList.add("mm-load-error");
  if (this.thumbnailDownloader) {
    this.thumbnailDownloader.onThumbnailLoadDone(this);
  }
};

MM.BrowserCell.thumbnailSizeAtZoomLevel = function(zoomLevel) {
  var minThumbnailSize = 64;
  var maxThumbnailSize = 256;
  return minThumbnailSize + zoomLevel * (maxThumbnailSize - minThumbnailSize);
};

MM.BrowserCell.widthAtZoomLevel = function(zoomLevel) {
  return MM.BrowserCell.thumbnailSizeAtZoomLevel(zoomLevel) + MM.BrowserCell.CellMargin * 2;
};

MM.BrowserCell.heightAtZoomLevel = function(zoomLevel) {
  return MM.BrowserCell.thumbnailSizeAtZoomLevel(zoomLevel) + MM.BrowserCell.CellMargin * 2 + MM.BrowserCell.LabelHeight + MM.BrowserCell.LabelMarginTop;
};

MM.BrowserCell.prototype.setSelected = function(selected) {
  this.element.classList.toggle("mm-selected", selected);
};

MM.BrowserCell.CellMargin = 10;
MM.BrowserCell.LabelHeight = 16;
MM.BrowserCell.LabelMarginTop = 4;

MM.BrowserCell.Priority = {
  LOW: 0,
  HIGH: 1
};

MM.ThumbnailDownloader = function() {
  this.highPriorityCells = [];
  this.lowPriorityCells = [];
  this.loadingCells = [];
  this.maxConcurrent = 5;
  this.maxConcurrentLowPriority = 3;
};

MM.ThumbnailDownloader.prototype.checkQueue = function() {
  this.loadHighPriorityCells();
  this.loadLowPriorityCells();
};

MM.ThumbnailDownloader.prototype.loadHighPriorityCells = function() {
  var numCells = Math.min(this.highPriorityCells.length, this.maxConcurrent - this.loadingCells.length);
  for (var i = 0; i < numCells; i++) {
    var cell = this.highPriorityCells.shift();
    this.loadingCells.push(cell);
    cell.loadThumbnail();
  }
};

MM.ThumbnailDownloader.prototype.loadLowPriorityCells = function() {
  var numCells = Math.min(this.lowPriorityCells.length, this.maxConcurrentLowPriority - this.loadingCells.length);
  for (var i = 0; i < numCells; i++) {
    var cell = this.lowPriorityCells.shift();
    this.loadingCells.push(cell);
    cell.loadThumbnail();
  }
};

MM.ThumbnailDownloader.prototype.promoteCell = function(cell) {
  var index = this.lowPriorityCells.indexOf(cell);
  if (index < 0) {
    return;
  }
  cell.priority = MM.BrowserCell.Priority.HIGH;
  this.lowPriorityCells.splice(index, 1);
  this.highPriorityCells.push(cell);
};

MM.ThumbnailDownloader.prototype.load = function(cell) {
  if (cell.thumbnailElement) {
    return;
  }
  cell.thumbnailDownloader = this;
  if (cell.priority === MM.BrowserCell.Priority.HIGH) {
    this.highPriorityCells.push(cell);
  } else {
    this.lowPriorityCells.push(cell);
  }
  this.checkQueue();
};

MM.ThumbnailDownloader.prototype.onThumbnailLoadDone = function(cell) {
  var index = this.loadingCells.indexOf(cell);
  this.loadingCells.splice(index, 1);
  this.checkQueue();
};

MM.ThumbnailDownloader.prototype.clear = function() {
  this.highPriorityCells = [];
  this.lowPriorityCells = [];
};

MM.BrowserView = function(dataSource, delegate) {
  this.element = document.createElement("div");
  this.element.classList.add("mm-image-browser-view");
  
  this.dataSource = dataSource;
  this.delegate = delegate;
  
  this.lastUpdateState = {
    time: 0,
    width: 0,
    height: 0,
    scrollTop: 0,
    zoomLevel: 0
  };
  
  this.cells = [];
  
  this.thumbnailDownloader = new MM.ThumbnailDownloader();
  
  this.zoomLevel = 1.0;
  
  this.timer = setInterval(this.onTimer.bind(this), 100);
  
  this.selection = [];
  
  this.onClick = this.onClick.bind(this);
  this.onDoubleClick = this.onDoubleClick.bind(this);
  this.element.addEventListener("click", this.onClick, false);
  this.element.addEventListener("dblclick", this.onDoubleClick, false);
};

MM.BrowserView.prototype.onClick = function(event) {
  var cellElement = MM.enclosingNodeOrSelfWithClass(event.target, "mm-image-browser-cell");
  if (cellElement) {
    for (var i = 0; i < this.cells.length; i++) {
      var cell = this.cells[i];
      if (cell && cell.element === cellElement) {
        this.select(i);
        break;
      }
    }
  }
};

MM.BrowserView.prototype.onDoubleClick = function(event) {
  var cellElement = MM.enclosingNodeOrSelfWithClass(event.target, "mm-image-browser-cell");
  if (cellElement) {
    for (var i = 0; i < this.cells.length; i++) {
      var cell = this.cells[i];
      if (cell && cell.element === cellElement) {
        this.delegate.didOpenItemAtIndex(i);
        break;
      }
    }
  }
};

MM.BrowserView.prototype.deselect = function() {
  for (var i=0; i < this.selection.length; i++) {
    this.selection[i].setSelected(false);
  }
  this.selection = [];
};

MM.BrowserView.prototype.select = function(index) {
  this.deselect();
  var cell = this.cells[index];
  cell.setSelected(true);
  this.selection = [cell];
};

MM.BrowserView.prototype.reloadData = function(index) {
  var width = this.element.offsetWidth;
  this.cells = new Array(this.dataSource.numberOfItems());
  var cellWidth = MM.BrowserCell.widthAtZoomLevel(this.zoomLevel);
  var cellHeight = MM.BrowserCell.heightAtZoomLevel(this.zoomLevel);
  var cellsPerRow = Math.floor(width / cellWidth);
  var height = cellHeight * Math.ceil(this.cells.length / cellsPerRow);
  this.element.style.height = height + "px";
};

MM.BrowserView.prototype.prepareCell = function(index) {
  if (this.cells[index]) {
    return;
  }
  var item = this.dataSource.itemAtIndex(index);
  console.assert(item, index);
  var cell = new MM.BrowserCell();
  cell.labelElement.textContent = item.title;
  cell.thumbnailSrc = item.thumbnail;
  this.element.appendChild(cell.element);
  this.cells[index] = cell;
  this.layoutCell(index);
};

MM.BrowserView.prototype.layoutCell = function(index) {
  var cell = this.cells[index];
  var cellWidth = MM.BrowserCell.widthAtZoomLevel(this.zoomLevel);
  var cellHeight = MM.BrowserCell.heightAtZoomLevel(this.zoomLevel);
  var cellsPerRow = Math.floor(this.lastUpdateState.width / cellWidth);
  var row = Math.floor(index / cellsPerRow);
  var col = index - row * cellsPerRow;
  cell.setZoomLevel(this.zoomLevel);
  cell.setPosition(col * cellWidth, row * cellHeight);
};

MM.BrowserView.prototype.setZoomLevel = function(zoomLevel) {
  if (this.zoomLevel === zoomLevel) {
    return;
  }
  this.zoomLevel = zoomLevel;
  for (var i = 0; i < this.cells.length; i++) {
    this.cells[i].setZoomLevel(this.zoomLevel);
  }
};

MM.BrowserView.prototype.scrollTop = function() {
  return Math.max(document.documentElement.scrollTop, document.body.scrollTop);
};

MM.BrowserView.prototype.setScrollTop = function(scrollTop) {
  document.documentElement.scrollTop = scrollTop;
  document.body.scrollTop = scrollTop;
};

MM.BrowserView.prototype.saveScrollTop = function() {
  this.savedScrollTop = this.scrollTop();
};

MM.BrowserView.prototype.restoreScrollTop = function() {
  this.setScrollTop(this.savedScrollTop);
};

MM.BrowserView.prototype.onTimer = function() {
  var scrollTop = this.scrollTop();
  var width = this.element.offsetWidth;
  var height = window.innerHeight;
  if (scrollTop !== this.lastUpdateState.scrollTop ||
      width !== this.lastUpdateState.width ||
      height !== this.lastUpdateState.height ||
      this.zoomLevel !== this.lastUpdateState.zoomLevel) {
    this.lastUpdateState.time = Date.now();
    this.lastUpdateState.width = width;
    this.lastUpdateState.height = height;
    this.lastScrollTop = scrollTop;
    this.thumbnailDownloader.clear();
    var cellWidth = MM.BrowserCell.widthAtZoomLevel(this.zoomLevel);
    var cellHeight = MM.BrowserCell.heightAtZoomLevel(this.zoomLevel);
    var cellsPerRow = Math.floor(width / cellWidth);
    var firstVisibleRow = Math.floor(scrollTop / cellHeight);
    var lastVisibleRow = Math.floor((scrollTop + height) / cellHeight);
    for (var row = firstVisibleRow; row <= lastVisibleRow; row++) {
      for (var col = 0; col < cellsPerRow; col++) {
        var index = row * cellsPerRow + col;
        if (index >= 0 && this.cells.length > index) {
          this.prepareCell(index);
          this.thumbnailDownloader.load(this.cells[index]);
        }
      }
    }
    var rowsPerScreen = Math.ceil(height / cellHeight);
    for (var offset = 1; offset <= rowsPerScreen; offset++) {
      var rowAbove = firstVisibleRow - offset;
      var rowBelow = lastVisibleRow + offset;
      for (var col = 0; col < cellsPerRow; col++) {
        var index = rowAbove * cellsPerRow + col;
        if (index >= 0 && this.cells.length > index) {
          this.prepareCell(index);
          this.thumbnailDownloader.load(this.cells[index]);
        }
        index = rowBelow * cellsPerRow + col;
        if (index >= 0 && this.cells.length > index) {
          this.prepareCell(index);
          this.thumbnailDownloader.load(this.cells[index]);
        }
      }
    }
  } else if (Date.now() - this.lastUpdateState.time > 600) {  
    var cellWidth = MM.BrowserCell.widthAtZoomLevel(this.zoomLevel);
    var cellHeight = MM.BrowserCell.heightAtZoomLevel(this.zoomLevel);
    var cellsPerRow = Math.floor(width / cellWidth);
    var firstVisibleRow = Math.floor(scrollTop / cellHeight);
    var lastVisibleRow = Math.floor((scrollTop + height) / cellHeight);
    for (var row = firstVisibleRow; row <= lastVisibleRow; row++) {
      for (var col = 0; col < cellsPerRow; col++) {
        var i = row * cellsPerRow + col;
        if (i >= 0 && this.cells.length > i) {
          this.thumbnailDownloader.promoteCell(this.cells[i]);
        }
      }
    }
  }
};

MM.BrowserDataSource = function() {
  this.mediaList = [];
};

MM.BrowserDataSource.prototype.numberOfItems = function() {
  return this.mediaList.length;
};

MM.BrowserDataSource.prototype.itemAtIndex = function(index) {
  return this.mediaList[index];
};

MM.ImageViewer = function() {
  this.element = document.createElement("div");
  this.element.classList.add("mm-image-viewer");
  
  this.image = new Image();
  this.image.src = "/master/000/000/000000006.jpg";
  this.element.appendChild(this.image);

  this.translateX = 0;
  this.translateY = 0;
  this.scale = 2.0;

  this.velocityX = 0;
  this.velocityY = 0;

  this.mode = MM.ImageViewer.Mode.IDLE;
  
  this.pointerDownClientX = 0;
  this.pointerDownClientY = 0;
  
  this.lastPointerClientX = 0;
  this.lastPointerClientY = 0;
  
  this.lastPinchCenterX = 0;
  this.lastPinchCenterT = 0;
  this.lastPinchDistance = 0;
  
  this.animationStep = 1000 / 60;
  
  this.savedScrollTop = 0;
  
  this.onMouseDown = this.onMouseDown.bind(this);
  this.onMouseMove = this.onMouseMove.bind(this);
  this.onMouseUp = this.onMouseUp.bind(this);
  this.onTouchStart = this.onTouchStart.bind(this);
  this.onTouchMove = this.onTouchMove.bind(this);
  this.onTouchEnd = this.onTouchEnd.bind(this);
  this.onMouseWheel = this.onMouseWheel.bind(this);
  
  this.element.addEventListener("mousewheel", this.onMouseWheel, false);
  this.element.addEventListener("mousedown", this.onMouseDown, false);
  this.element.addEventListener("touchstart", this.onTouchStart, false);
};

MM.ImageViewer.Mode = {
  IDLE: 0,
  TRACKING: 1,
  INERTIA: 2,
  ZOOMING: 3
};

MM.ImageViewer.TrackingStartThreshold = 5;
MM.ImageViewer.InertiaAnimationStep = 1000 / 60;
MM.ImageViewer.InertiaDampingFactor = 0.95;

MM.ImageViewer.prototype.layout = function() {
  this.image.style.WebkitTransform = new WebKitCSSMatrix()
      .scale(this.scale, this.scale, 0)
      .translate(this.translateX, this.translateY, 0)
      .translate(0, 0, 1);
};

MM.ImageViewer.prototype.onMouseWheel = function(event) {
  var pos = MM.cumulativeOffset(this.element);
  var wx = event.pageX - pos[0];
  var wy = event.pageY - pos[1];
  this.zoom(event.deltaY > 0 ? 0.8 : 1.2, wx, wy);
};

MM.ImageViewer.prototype.zoom = function(zoomFactor, zoomCenterX, zoomCenterY) {
  this.scale *= zoomFactor;
  this.translateX += (1 - zoomFactor) * zoomCenterX / this.scale;
  this.translateY += (1 - zoomFactor) * zoomCenterY / this.scale;
  this.layout();
};

MM.ImageViewer.prototype.onTouchStart = function(event) {
  if (this.mode === MM.ImageViewer.Mode.INERTIA) {
    this.stopInertiaAnimation();
  }
  if (event.touches.length >= 2) {
    this.startZooming(event);
  }
  var primaryTouch = event.touches[0];
  primaryTouch.timeStamp = event.timeStamp;
  this.pointerDownClientX = primaryTouch.clientX;
  this.pointerDownClientY = primaryTouch.clientY;
  this.lastPointerTimestamp = primaryTouch.timeStamp;
  this.lastPointerClientX = primaryTouch.clientX;
  this.lastPointerClientY = primaryTouch.clientY;
  this.element.addEventListener("touchmove", this.onTouchMove, false);
  this.element.addEventListener("touchend", this.onTouchEnd, false);
  event.preventDefault();
  event.stopPropagation();
};

MM.ImageViewer.prototype.onTouchMove = function(event) {
  var primaryTouch = event.touches[0];
  primaryTouch.timeStamp = event.timeStamp;
  if (this.mode === MM.ImageViewer.Mode.IDLE) {
    this.startTrackingIfNecessary(primaryTouch);
  }
  if (this.mode === MM.ImageViewer.Mode.TRACKING) {
    this.executeTracking(primaryTouch);
  } else if (this.mode === MM.ImageViewer.Mode.ZOOMING) {
    this.executeZooming(event);
  }
  event.preventDefault();
  event.stopPropagation();
};

MM.ImageViewer.prototype.onTouchEnd = function(event) {
  console.log("onTouchEnd", this.mode);
  if (this.mode === MM.ImageViewer.Mode.TRACKING) {
    this.startInertiaAnimation();
  } else {
    this.mode = MM.ImageViewer.Mode.IDLE;
  }
  this.element.removeEventListener("touchmove", this.onTouchMove, false);
  this.element.removeEventListener("touchend", this.onTouchEnd, false);
  event.preventDefault();
  event.stopPropagation();
};

MM.ImageViewer.prototype.onMouseDown = function(event) {
  if (this.mode === MM.ImageViewer.Mode.INERTIA) {
    this.stopInertiaAnimation();
  }
  this.pointerDownClientX = event.clientX;
  this.pointerDownClientY = event.clientY;
  this.lastPointerTimestamp = event.timeStamp;
  this.lastPointerClientX = event.clientX;
  this.lastPointerClientY = event.clientY;
  window.addEventListener("mousemove", this.onMouseMove, false);
  window.addEventListener("mouseup", this.onMouseUp, false);
  event.preventDefault();
  event.stopPropagation();
};

MM.ImageViewer.prototype.onMouseMove = function(event) {
  if (this.mode === MM.ImageViewer.Mode.IDLE) {
    this.startTrackingIfNecessary(event);
  }
  if (this.mode === MM.ImageViewer.Mode.TRACKING) {
    this.executeTracking(event);
  }
  event.preventDefault();
  event.stopPropagation();
};

MM.ImageViewer.prototype.onMouseUp = function(event) {
  if (this.mode === MM.ImageViewer.Mode.TRACKING) {
    this.startInertiaAnimation();
  } else {
    this.mode = MM.ImageViewer.Mode.IDLE;
  }
  window.removeEventListener("mousemove", this.onMouseMove, false);
  window.removeEventListener("mouseup", this.onMouseUp, false);
  event.preventDefault();
  event.stopPropagation();
};

MM.ImageViewer.prototype.startInertiaAnimation = function() {
  this.mode = MM.ImageViewer.Mode.INERTIA;
  this.inertiaAnimationTimer = setTimeout(this.executeInertiaAnimationStep.bind(this), MM.ImageViewer.InertiaAnimationStep);
};

MM.ImageViewer.prototype.executeInertiaAnimationStep = function() {
  if (this.mode !== MM.ImageViewer.Mode.INERTIA) {
    return;
  }
  this.velocityX *= MM.ImageViewer.InertiaDampingFactor;
  this.velocityY *= MM.ImageViewer.InertiaDampingFactor;
  this.translateX += this.velocityX * MM.ImageViewer.InertiaAnimationStep;
  this.translateY += this.velocityY * MM.ImageViewer.InertiaAnimationStep;

  var minX = this.element.offsetWidth / this.scale - this.image.naturalWidth;
  var minY = this.element.offsetHeight / this.scale - this.image.naturalHeight;
  var maxX = 0;
  var maxY = 0;

  if (minX > 0) {
    minX = minX / 2;
    maxX = maxX / 2;
  }
  
  if (minY > 0) {
    minY = minY / 2;
    maxY = maxY / 2;
  }

  var penetrationX = Math.max(this.translateX - maxX, 0);
  var penetrationY = Math.max(this.translateY - maxY, 0);
  penetrationX = Math.min(this.translateX - minX, penetrationX);
  penetrationY = Math.min(this.translateY - minY, penetrationY);

  if (penetrationX !== 0) {
    if (penetrationX * this.velocityX > 0) {
      this.velocityX -= penetrationX * 0.06 / MM.ImageViewer.InertiaAnimationStep;
    } else {
      this.velocityX = -penetrationX * 0.16 / MM.ImageViewer.InertiaAnimationStep;
    }
  }
  
  if (penetrationY !== 0) {
    if (penetrationY * this.velocityY > 0) {
      this.velocityY -= penetrationY * 0.06 / MM.ImageViewer.InertiaAnimationStep;
    } else {
      this.velocityY = -penetrationY * 0.16 / MM.ImageViewer.InertiaAnimationStep;
    }
  }

  this.layout();
  if (Math.abs(penetrationX - 0) < 0.5 &&
      Math.abs(penetrationY - 0) < 0.5 &&
      Math.abs(this.velocityX * MM.ImageViewer.InertiaAnimationStep) < 1 &&
      Math.abs(this.velocityY * MM.ImageViewer.InertiaAnimationStep) < 1) {
    this.stopInertiaAnimation();
  } else {
    this.inertiaAnimationTimer = setTimeout(this.executeInertiaAnimationStep.bind(this), MM.ImageViewer.InertiaAnimationStep);
  }
};

MM.ImageViewer.prototype.stopInertiaAnimation = function() {
  this.mode = MM.ImageViewer.Mode.IDLE;
  clearTimeout(this.inertiaAnimationTimer);
};

MM.ImageViewer.prototype.startZooming = function(event) {
  this.mode = MM.ImageViewer.Mode.ZOOMING;
  var primaryTouch = event.touches[0];
  var secondaryTouch = event.touches[1];
  var pos = MM.cumulativeOffset(this.element);
  var primaryTouchX = primaryTouch.pageX - pos[0];
  var primaryTouchY = primaryTouch.pageY - pos[1];
  var secondaryTouchX = secondaryTouch.pageX - pos[0];
  var secondaryTouchY = secondaryTouch.pageY - pos[1];
  this.lastPinchDistance = Math.pow(primaryTouch.clientX - secondaryTouch.clientX, 2) + Math.pow(primaryTouch.clientY - secondaryTouch.clientY, 2);
  this.lastPinchCenterX = (primaryTouchX + secondaryTouchX) / 2;
  this.lastPinchCenterY = (primaryTouchY + secondaryTouchY) / 2;
};

MM.ImageViewer.prototype.executeZooming = function(event) {
  var primaryTouch = event.touches[0];
  var secondaryTouch = event.touches[1];
  var pos = MM.cumulativeOffset(this.element);
  var primaryTouchX = primaryTouch.pageX - pos[0];
  var primaryTouchY = primaryTouch.pageY - pos[1];
  var secondaryTouchX = secondaryTouch.pageX - pos[0];
  var secondaryTouchY = secondaryTouch.pageY - pos[1];
  var pinchDistance = Math.pow(primaryTouchX - secondaryTouchX, 2) + Math.pow(primaryTouchY - secondaryTouchY, 2);
  var pinchCenterX = (primaryTouchX + secondaryTouchX) / 2;
  var pinchCenterY = (primaryTouchY + secondaryTouchY) / 2;
  this.translateX += (pinchCenterX - this.lastPinchCenterX) / this.scale;
  this.translateY += (pinchCenterY - this.lastPinchCenterY) / this.scale;
  this.zoom(pinchDistance / this.lastPinchDistance, pinchCenterX, pinchCenterY);
  this.lastPinchCenterX = pinchCenterX;
  this.lastPinchCenterY = pinchCenterY;
  this.lastPinchDistance = pinchDistance;
};

MM.ImageViewer.prototype.startTrackingIfNecessary = function(event) {
  if (Math.abs(event.clientX - this.pointerDownClientX) >= MM.ImageViewer.TrackingStartThreshold ||
      Math.abs(event.clientY - this.pointerDownClientY) >= MM.ImageViewer.TrackingStartThreshold) {
    this.mode = MM.ImageViewer.Mode.TRACKING;
  }
};

MM.ImageViewer.prototype.executeTracking = function(event) {
  var deltaX = (event.clientX - this.lastPointerClientX) / this.scale;
  var deltaY = (event.clientY - this.lastPointerClientY) / this.scale;
  var deltaT = event.timeStamp - this.lastPointerTimestamp;
  this.velocityX = deltaX / deltaT * 2.5;
  this.velocityY = deltaY / deltaT * 2.5;
  this.translateX += (event.clientX - this.lastPointerClientX) / this.scale;
  this.translateY += (event.clientY - this.lastPointerClientY) / this.scale;
  this.lastPointerTimestamp = event.timeStamp;
  this.lastPointerClientX = event.clientX;
  this.lastPointerClientY = event.clientY;
  this.layout();
};

MM.BrowserController = function(mediaList) {
  this.dataSource = new MM.BrowserDataSource();
  this.dataSource.mediaList = mediaList;
  this.view = new MM.BrowserView(this.dataSource, this);
  document.body.appendChild(this.view.element);
  this.view.reloadData();
  this.viewer = new MM.ImageViewer();
  
  this.onWindowKeyDown = this.onWindowKeyDown.bind(this);
  window.addEventListener("keydown", this.onWindowKeyDown, false);
};

MM.BrowserController.prototype.onWindowKeyDown = function(event) {
  if (event.which === 27) {
    this.showBrowser();
  }
};

MM.BrowserController.prototype.didOpenItemAtIndex = function(index) {
  this.viewer.image.src = this.dataSource.itemAtIndex(index).master;
  this.showViewer();
};

MM.BrowserController.prototype.showBrowser = function(index) {
  document.body.removeChild(this.viewer.element);
  document.body.appendChild(this.view.element);
  this.view.restoreScrollTop();
};

MM.BrowserController.prototype.showViewer = function(index) {
  this.view.saveScrollTop();
  document.body.removeChild(this.view.element);
  document.body.appendChild(this.viewer.element);
  this.viewer.element.style.width = window.innerWidth + "px";
  this.viewer.element.style.height = window.innerHeight + "px";
};

var xhr = new XMLHttpRequest();
xhr.open('GET', '/media');
xhr.onload = function(e) {
  browserController = new MM.BrowserController(JSON.parse(this.response));
}
xhr.send();


