from mm import app, db
from mm.models import Media, Tag
from flask.ext.script import Manager, Server
import os

manager = Manager(app)

manager.add_command('start', Server(host='0.0.0.0'))

def add_file(filename):
    media = Media()
    media.hash = Media.get_file_hash(filename)
    media.extension = Media.get_file_extension(filename)
    if media.extension is None:
        print "%s is an unsupported file." % filename
        return None
    media.title = os.path.splitext(os.path.basename(filename))[0]
    media.insert()
    if media.id is None:
        return
    media.prepare_master(filename)
    media.prepare_thumbnail(filename)

@manager.command
def setup():
    print "Creating database tables."
    print app.config['DATABASE']
    db.init()

@manager.command
def add(dir):
    for (path, dirs, files) in os.walk(dir):
        for filename in files:
            add_file(os.path.join(path, filename))
    db.commit()

if __name__ == "__main__":
    manager.run()
